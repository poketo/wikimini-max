// ==UserScript==
// @name        Wikimini MAX
// @namespace   Violentmonkey Scripts
// @match       https://fr.wikimini.org/wiki/*
// @match       https://fr.wikimini.org/w/*
// @require     https://rawcdn.githack.com/glauberramos/vanilla-emoji-picker/194c8a40ec5169c9871a4c786f1583f132f6e030/dist/emojiPicker.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.6.0/umd/popper.min.js
// @require     https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js
// @updateURL   https://gitlab.com/poketo/wikimini-max/-/raw/master/wikiminimax.user.js
// @downloadURL https://gitlab.com/poketo/wikimini-max/-/raw/master/wikiminimax.user.js
// @grant       GM_registerMenuCommand
// @grant       GM_unregisterMenuCommand
// @grant       GM_setValue
// @grant       GM_getValue
// @version     1.0
// @author      Poketo
// @description 19/06/2020 à 17:03:04
// ==/UserScript==

function getElementByXpath(path) {
  return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
}

var mobilenavtoggled = 0

const emlist = {
  ":wikiboo:" : "[[File:Wikiboo_stand.png|19px]]"
}

function mnpls_newemojis(){
  alert("Liste des Emojis+ :\n:wikiboo:")
}

function url_content(url){
  return $.get(url);
}

function get_elements_by_inner(word) {
    res = []
    elems = [...document.querySelectorAll()];
    elems.forEach((elem) => { 
        if(elem.outerHTML.includes(word)) {
            res.push(elem)
        }
    })
    return(res)
}

if (location.href == "https://fr.wikimini.org/wiki/Accueil"){
  try {
    $.get("https://fr.wikimini.org/wiki/Special:Qui_est_en_ligne",function(data){ 
      var dernierlien01 = document.querySelector("#mw-content-text div a[href='/wiki/Sp%C3%A9cial:Page_au_hasard']").parentNode
      console.log(data)
      dernierlien01.outerHTML = dernierlien01.outerHTML + "<h2>Qui est en ligne?</h2>" + $(data).find("#mw-content-text").html()
    });
  }
  catch(error){console.log(error)}
}
  
setInterval(function(){
  //EMOJI+
  document.querySelectorAll("textarea").forEach(function(eelmnt) {
    for (const [ key, value ] of Object.entries(emlist)) {
        eelmnt.value = eelmnt.value.replace(key,value)
    }
  });
}, 250);

//ChickenSmoothie.com
document.querySelectorAll("[id^='chickensmoothie-']").forEach(function(eelmnt) {
    try {
      //16
      eelmnt.outerHTML = "<a href='https://www.chickensmoothie.com/pet/" + eelmnt.getAttribute("id").substring(16) + ".html'><img src='https://www.chickensmoothie.com/pet/" + eelmnt.getAttribute("id").substring(16) + "&trans=1.jpg'>"
    }
    catch(error){}
  });

if (document.location.href.startsWith("https://fr.wikimini.org/wiki/Sp%C3%A9cial:AWCforum/")){
  if (document.querySelector(".post_box") != null){
    console.log("y")
    if (document.querySelector(".forumtoolbar") != null){
      document.querySelector(".forumtoolbar").innerHTML = document.querySelector(".forumtoolbar").innerHTML + " <a id='newemojibutton'>Emoji+</a>"
      document.querySelector("#newemojibutton").addEventListener("click", mnpls_newemojis);
    }
  }
  else{console.log("gpatrouvé")}
}

document.querySelectorAll("textarea").forEach(function(elmnt) {
  elmnt.setAttribute("data-emoji-picker","true");
});

new EmojiPicker();

//CONFIG PAR DEFAUT DE L'AV
if (GM_getValue("mnpls_av") == null){
  GM_setValue("mnpls_av",0)
}

//Nombre de virus bloqués
if (GM_getValue("mnpls_av_nmbr") == null){
  GM_setValue("mnpls_av_nmbr",0)
}

function mnpls_av_kw(){
  GM_setValue("mnpls_av_nmbr",GM_getValue("mnpls_av_nmbr") + 1)
  alert("Un virus a été bloqué par l'antivirus : Virus de faille de recherche\nCeci est le " + GM_getValue("mnpls_av_nmbr") + "ème virus bloqué")
}

//DETECTION AV
if (GM_getValue("mnpls_av") == 1){
  document.querySelectorAll("a[href^='https://fr.wikimini.org/wiki/Special:AWCforum/search/s?kw=']").forEach(function(kwelmnt) {
    kwelmnt.setAttribute('id','urldangereuse');
    kwelmnt.removeAttribute('href');
    kwelmnt.addEventListener('click', mnpls_av_kw);
  });
}

//METTRE LA NAVIGATION HTML5 SANS SWIFFYCONTAINER
setInterval(function(){
  if (mobilenavtoggled == 0) {
    try {
      document.querySelector("#swiffycontainer").setAttribute("style","display:none")
      var mobilenav = document.querySelector("#mobileNav")
      mobilenav.setAttribute("id","mobileNavShown")
      mobilenav.removeAttribute("class")
      mobilenavtoggled = 1
    }
    catch(error){
      console.log(error)
    }
  }
  }, 500);

//AJOUTER A UN LI
function mnpls_addli(iul, ili) {
  iul.innerHTML = iul.innerHTML + "<li style='list-style-type:none;list-style-image:none;'>" + ili + "</li>"
}

//ICONE PSEUDO
function mnpls_iconpseudo() {
  try {
  console.log("icon called")
  mnpls_iconpseudo_element = document.querySelector("div.mw-body.container div.row.fluid-candidate div.content-bottom div.box-bottom div ul li#pt-userpage a")
  console.log(mnpls_iconpseudo_element)
  mnpls_iconpseudo_element.innerHTML = mnpls_iconpseudo_element.innerHTML + " <span style='color:#ccff00'>[</span><span style='color:#00ff66'>M</span><span style='color:#0066ff'>A</span><span style='color:#cc00ff'>X</span><span style='color:#ff0000'>]</span>"
  }
  catch(error){
    console.log("icon error")
  }
}


//ICONE CE QUE TU PEUT FAIRE
function mnpls_iconcqtpf() {
  try {
  console.log("cqtpf icon called")
  mnpls_cqtpf_element = document.querySelector(".content-bottom h5")
  console.log(mnpls_cqtpf_element)
  mnpls_cqtpf_element.innerHTML = mnpls_cqtpf_element.innerHTML + " <span style='color:#ccff00'>[</span><span style='color:#00ff66'>M</span><span style='color:#0066ff'>A</span><span style='color:#cc00ff'>X</span><span style='color:#ff0000'>]</span>"
  }
  catch(error){
    console.log("cqtpf icon error")
  }
}



if (location.href.startsWith("https://fr.wikimini.org/wiki/Sp%C3%A9cial:AWCforum")) {
  console.log("d forum")
  setTimeout(function(){
    mnpls_iconcqtpf()
    mnpls_iconpseudo()
  },3500)
}

else if (location.href.startsWith("https://fr.wikimini.org/wiki/")) {
  //Vérification de si ça fonctionne
  console.log("d")
  setTimeout(function(){
    mnpls_iconcqtpf()
    mnpls_iconpseudo()
  },3500)
  //Bouton Signaler
  setTimeout(function(){
    var mnpls_optionbox = document.querySelector(".skin-wikimini.action-view div.mw-body.container div#fluid-row.row.fluid-candidate div#fluid-sidebar.col-md-5.text-center div.option-box.text-center div.text-left div.content ul")
    console.log(mnpls_optionbox)
    mnpls_addli(mnpls_optionbox, "<b><a href='https://fr.wikimini.org/wiki/Discussion_Wikimini:Alertes'>Signaler</a></b>")
  }, 3500);
  //Toujours afficher le bouton 'Modifier le Wikicode' même si le script du site ne veut pas l'afficher
  setInterval(function(){
    try{
    var mnpls_caedit = document.querySelector(".ca-edit")
    mnpls_caedit.setAttribute("class","ca-edit-shown")
    mnpls_caedit.setAttribute("style","list-style-image:url(/w/skins/Wikimini/resources/images/icon-ca-edit.gif?a0f68);")
    }
    catch(error){}
  }, 1000);
}

if (location.href == "https://fr.wikimini.org/wiki/Sp%C3%A9cial:AWCforum/member_options/editsig"){
  setTimeout(function(){
    try {
      document.querySelector("#wpTextbox1").removeAttribute("onkeydown")
      document.querySelector("#wpTextbox1").removeAttribute("onkeyup")
    }
    catch(error){}
  },50)
}

function mnpls_b_avatar() {
  alert("Pour mettre un avatar sur Wikimini, il faudra :\nMettre en ligne l'avatar sur le site\nValidez\nCopiez le lien dans la boîte a texte (ça commence par http://ibb.co/)\nPuis collez le dans la barre d'adresse\nPuis faites clique droit et 'Copier l'adresse de l'image'\nPuis mettez le sur wikimini en mettant http a la place de https")
  location.href = "https://imgbb.com/"
}

function mnpls_antivirus_toggle() {
  if (GM_getValue("mnpls_av") == 0) {
    alert("L'antivirus a été activé, toutes les failles et fausses alertes ne marcheront plus")
    GM_setValue("mnpls_av", 1)
    location.reload()
  }
  else if (GM_getValue("mnpls_av") == 1) {
    alert("L'antivirus a été désactivé, toutes les failles et fausses alertes remarchent")
    GM_setValue("mnpls_av", 0)
    location.reload()
  }
}
// QUI EST EN LIGNE
//<a href="/wiki/Sp%C3%A9cial:Page_au_hasard" title="Spécial:Page au hasard">Au hasard</a>




//BOUTONS

//GM_unregisterMenuCommand("[1]🖼️Avatar")
GM_registerMenuCommand("[1]🖼️Avatar", mnpls_b_avatar)

//GM_unregisterMenuCommand("[2]🛡️Désactiver l'antivirus")
//GM_unregisterMenuCommand("[2]🛡️Activer l'antivirus")

if (GM_getValue("mnpls_av") == 0) {
    GM_registerMenuCommand("[2]🛡️Activer l'antivirus", mnpls_antivirus_toggle)
}
else if (GM_getValue("mnpls_av") == 1) {
    GM_registerMenuCommand("[2]🛡️Désactiver l'antivirus", mnpls_antivirus_toggle)
}

if (GM_getValue("mnpls_av") == 1) {
    GM_registerMenuCommand("[INFO AV]🛡️" + GM_getValue("mnpls_av_nmbr") + " virus bloqués" , mnpls_antivirus_toggle)
}
